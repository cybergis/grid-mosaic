#!/usr/bin/env/ python
import fiona
import argparse
from subprocess import call
from shapely.geometry import shape


def createMosaic(tileIndex, vector, output):
    """
    Returns a virtual raster made up of specified tiles.

    The input vector is overlaid against the grid. Then the tiles that are 
    in the intersection of the two are returned as a virtual raster. 

    Parameters
    ----------
    grid:       - Grid geoJSON
    vector:     - Input vector to overlap against grid
    output:     - Name of the output virtual raster

    """
    with open('list_of_tiles.txt', 'wb') as wf:
        for layername in fiona.listlayers(vector):
            with fiona.open(vector, layer=layername) as source:
                with fiona.open(tileIndex) as grid:
                    # Process all the records in the input vector
                    for f in source:
                        # Shapely's shape function to turn a dictionary object
                        # into a Shapely Polygon
                        geom = shape(f['geometry'])
                        # Process all the records in the tile index
                        for g in grid:
                            poly = shape(g['geometry'])
                            if geom.intersects(poly):
                                filename = g['properties']['location']
                                wf.write(filename+"\n")
    call(["gdalbuildvrt", output, "-input_file_list", "list_of_tiles.txt"])
 

def main():
    parser = argparse.ArgumentParser(
        usage = 'python %s input output' % __file__,
        formatter_class = argparse.RawDescriptionHelpFormatter,
        description=__doc__)

    parser.add_argument("filename", type=str,
        help="The geoJSON tile index")

    parser.add_argument("vector", type=str,
        help="Vector file to overlay against the tile index")

    parser.add_argument("output", type=str,
        help="Name of the output virtual raster")

    args = parser.parse_args()
    createMosaic(args.filename, args.vector, args.output)
#    test(args.filename, args.vector, args.output)


if __name__ == "__main__" :
    main()
