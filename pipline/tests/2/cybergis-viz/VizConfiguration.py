import ConfigParser
import os.path
from Singleton import Singleton


class VizConfiguration(Singleton):
    """
    A class to access the configuration of cyvergis-viz
    All the members should be taken as read-only and access through associated get functions
    Any assignment operation is prohibited. Please see the test code for usages.
    @author: Guofeng Cao
    @see: Singleton.py  
    """
    def __init__(self):
        super(VizConfiguration, self).__init__()                
        self._config = ConfigParser.ConfigParser()
        
        self._config_file = os.path.dirname(__file__) + "/cybergis-viz.properties.gateway-viz1"
        
        self._config.read(self._config_file)
        
        self._appPath = self._config.get("DEFAULT","PATH")
        self._tempPath=self._config.get("DEFAULT","TMP")
        self._appName = self._config.get("DEFAULT","APP")
        self._logLevl = self._config.get("DEFAULT","LOGLEVEL")
        
        self._timeInterval= int(self._config.get("DEFAULT","INTERVAL"))
        
        self._logPath=self._config.get("Path","Log")        
        
        self._geoTIFFPath= self._config.get("Path","GeoTIFF")
        self._sldPath=self._config.get("Path","SLD")
    
        self._controllerDB_Host=self._config.get("ControllerDB","Host")
        self._controllerDB_Port=self._config.get("ControllerDB","Port")
        self._controllerDB_UserName=self._config.get("ControllerDB","UserName")
        self._controllerDB_Password=self._config.get("ControllerDB","Password")
        self._controllerDB_DBName=self._config.get("ControllerDB","DB_Name")
    
        self._vectorDB_Host=self._config.get("VectorDB","Host")
        self._vectorDB_Port=self._config.get("VectorDB","Port")
        self._vectorDB_UserName=self._config.get("VectorDB","UserName")
        self._vectorDB_Password=self._config.get("VectorDB","Password")
        self._vectorDB_DBName=self._config.get("VectorDB","DB_Name")
        
        self._geoServer_Host=self._config.get("Geoserver","Host")
        self._geoServer_Port=self._config.get("Geoserver","Port")
        self._geoServer_UserName=self._config.get("Geoserver","UserName")
        self._geoServer_Password=self._config.get("Geoserver","Password")
        self._geoServer_PasswordFile=self._config.get("Geoserver","PasswordFile")
        self._geoServer_Store=self._config.get("Geoserver","Store")
        self._geoServer_Namespace=self._config.get("Geoserver","Namespace")
        
        self._resultName = self._config.get("Results","Name")
        self._resultURL=self._config.get("Results","URL")
        self._resultMountDir=self._config.get("Results","MountDir")
        
    def getAppPath(self):
        return self._appPath
    
    def getTempPath(self):
        return self._tempPath
    
    def getLogPath(self):
        return self._logPath
    
    def getAppName(self):
        return self._appName
    
    def getLogLevel(self):
        return self._logLevl
    
    def getTimeInterval(self):
        return self._timeInterval
    
    def getGeoTIFFPath(self):
        return self._geoTIFFPath
    
    def getSLDPath(self):
        return self._sldPath
    
    """
     controller related
    """
    
    def getControllerDBHost(self):
        return self._controllerDB_Host
    
    def getControllerDBPort(self):
        return self._controllerDB_Port
    
    def getControllerDBUserName(self):
        return self._controllerDB_UserName
    
    def getControllerDBPassword(self):
        return self._controllerDB_Password
    
    def getControllerDBName(self):
        return self._controllerDB_DBName
    
    """
     vector database
    """
    
    def getVectorDBHost(self):
        return self._vectorDB_Host
    
    def getVectorDBPort(self):
        return self._vectorDB_Port
    
    def getVectorDBUserName(self):
        return self._vectorDB_UserName
    
    def getVectorDBPassword(self):
        return self._vectorDB_Password
    
    def getVectorDBName(self):
        return self._vectorDB_DBName
    """
      geoserver related 
    """
    
    def getGeoserverHost(self):
        return self._geoServer_Host
    
    def getGeoserverPort(self):
        return self._geoServer_Port
    
    def getGeoserverUserName(self):
        return self._geoServer_UserName
    
    def getGeoserverPassword(self):
        return self._geoServer_Password
    
    def getGeoserverStore(self):
        return self._geoServer_Store
    
    def getGeoserverNamespace(self):
        return self._geoServer_Namespace
    """
      result related parameters
    """
    def getResultName(self):
        return self._resultName
    def getResultURL(self):
        return self._resultURL
    def getResultMountDir(self):
	return self._resultMountDir

if __name__ == "__main__":
    test=VizConfiguration.getInstance()
    print test.getTempPath()
    print test.getLogPath()
    
        
