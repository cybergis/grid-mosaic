from RasterDataset import RasterDataset,FileRasterDataset
from VizConfiguration import VizConfiguration
from GeoserverConnector import GeoserverConnector
from VectorDataset import OgrVectorDataset
import os
import osr
import sys
import glob 
from osgeo import gdal, gdalconst

class VizMain:
    @classmethod  
    def _prepareNedKDE(cls, tiffile):
        geoserver = GeoserverConnector.getInstance()
        geoserver.createWorkspace("UsgsNedDataApp")
        layerName = os.path.splitext(tiffile)[0].split("/")[-1]
        dataset = FileRasterDataset(tiffile)
        ret = geoserver.addLayer(dataset,layerName,None, "flukde")
        if ret == False:
            print "Cannot add layer {}".format(layerName)

    @classmethod
    def startProxy(cls, tiffile):
        VizMain._prepareNedKDE(tiffile)

def main(tiffile):
    VizMain.startProxy(tiffile)

if __name__ == "__main__":
    main(sys.argv[1])
