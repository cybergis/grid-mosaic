gdalwarp -cutline /gpfs_scratch/ned_data_boundaries/State/geojson/MA.geojson -crop_to_cutline -of VRT            /gpfs_scratch/usgs/ned10m/ned10m.vrt MA.vrt
gdalwarp -t_srs "EPSG:26957" -of VRT MA.vrt MA_repro.vrt
gdalwarp MA_repro.vrt MA.tif
gdaldem hillshade MA.tif MA_hillshade.tif
python cybergis-viz/VizMain.py MA_hillshade.tif
rm MA_hillshade.tif
rm MA.vrt MA_repro.vrt MA.tif
