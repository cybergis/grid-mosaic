from Dataset import Dataset
#import ogr,osr
import osgeo.ogr as ogr
import osgeo.osr as osr
import locale
import logging
import csv
import os.path,sys
from VizConfiguration import VizConfiguration
from gdalconst import GA_Update


class VectorDataset(Dataset):
    """
        an abstract class for general vector dataset operations, file-based or database-based
        @author Guofeng Cao
        @todo: to account for the vector files such as shp and tab
    """
    def __init__(self,dsName=None, dtName=None):
        Dataset.__init__(self,dsName,dtName)
    def open(self,dsName,dtName):
        Dataset.open(self, dsName, dtName)
    def close(self):
        Dataset.close()
    def query(self,bb,filter):
        raise NotImplemented

class OgrVectorDataset(VectorDataset):
    """
        an implementation of vector dataset based on GDAL/OGR library
        @author Guofeng Cao
        @todo: 
    """
    
    def __init__(self, dsName=None, dtName=None):
        VectorDataset.__init__(self,dsName, dtName)
        self._mins =[]
        self._maxs = []
        
        if not dsName is None and not dtName is None:
            self.open(dsName, dtName)
        
    """
        open an existing dataset via database connection or local file name
        For local file, dsName is the file name
        For dataset base, dsName is a database connection, and the name convention follows OGR format:
        @see http://www.gdal.org/ogr/ogr_formats.html 
    """
    def open(self, dsName, dtName, filetype = 1):
        # if already open, return
        if not self._datasource is None or not self._dataset is None:
            return  
        VectorDataset.open(self,dsName,dtName )
        self._filetype = filetype
        ogr.RegisterAll()
        # the locale encoding stuff is needed for gdal <1.8.0
        dtName = dtName.encode(locale.getpreferredencoding())
        self._datasource = ogr.Open(dsName.encode(locale.getpreferredencoding()))
        #self._datasource = ogr.Open(dsName)
        if self._datasource is None:
            self._logger.error("Failed to initialize data source: %s\n"%dsName)
            return False
        self._dataset = self._datasource.GetLayer(dtName)
        if self._dataset is None:
            self._logger.error("Failed to initialize dataset: %s\n"%dtName)
            return False
        
        typeFeat = self._dataset.GetLayerDefn().GetGeomType()
        # mapping ogr format to our format, partially support
        if typeFeat == ogr.wkbPoint:
            self._type = Dataset.POINT
        elif typeFeat ==ogr.wkbLineString:
            self._type = Dataset.LINE
        elif typeFeat == ogr.wkbPolygon:
            self._type = Dataset.POLYGON
        srs = self._dataset.GetSpatialRef()
        #if srs is not None:
        #    self._projection = srs.exportToWkt()
            
                
    def close(self):
        VectorDataset.close()
        self._mins = []
        self._maxs= []
        
    
    '''
        to be added according to future spatial query needs
    '''
    def query(self,bb,filter):
        return
        
    """
    input a csv file into different format, the default is postgresql
    """
    def importFromFile(self,fileName, prjFile = None, dtName = None,toFormat = "PostgreSQL"):
        if fileName is None or not os.path.isfile(fileName):
            return 
        if dtName is None:
            pathName, extension = os.path.splitext(fileName)
            dtName  = os.path.split(pathName)[-1]
              
        driverName = toFormat
        drv = ogr.GetDriverByName( driverName )
        self._type = Dataset.POINT
	self._filetype = 1 #indicated as database
        if drv is None:
            self._logger.error ("%s driver not available.\n" % driverName)
            sys.exit(1)
        self._dsName  = "PG:dbname=%s host=%s user=%s password=%s port=%s"%(self._config.getVectorDBName(),self._config.getVectorDBHost(), self._config.getVectorDBUserName(),self._config.getVectorDBPassword(),self._config.getVectorDBPort())
        """
        it is strange, ogr can't open the datasource in read-only mode (default mode). 
        """
        self._datasource = ogr.Open(self._dsName,GA_Update)
        if self._datasource is None:
            self._logger.error("Fail to open PG:dbname=%s user=%s password=%s.\n"%(self._config.getVectorDBName(),self._config.getVectorDBUserName(),self._config.getVectorDBPassword()))
            sys.exit(1)
        self._dtName = dtName
        
        srs= None
        epsgCode = 4326
        if not prjFile is None:
            prjPathName, extension = os.path.splitext(prjFile)
            try:
                epsgCode = int(os.path.split(prjPathName)[-1])
            except ValueError:
                self._logger.info("The projection files is not epsg coded")
            srs = osr.SpatialReference()
            srs.ImportFromEPSG(epsgCode)
            
            
        self._dataset = self._datasource.GetLayer(self._dtName)
        if not self._dataset is None:
            self._datasource.DeleteLayer(self._dataset.GetName())
        '''
         The postgis support of geoserver does't seems decent particularly for the projection for today (Jan.12.2012) at least, 
         It only recognize the srid in geometry_columns table and doesn't support the foreign key in spatial_ref_sys. 
         In other words, it doesn't acutally care what has been written in spatial_refs_sys table. To circumvent this, 
         we have to manually set change the srid, which is supposed to be foreign key of table spatial_ref_sys, to espg code
         Please keep this in mind and make changes accordingly in the future upgrading of geoserver.
         '''   
        self._dataset = self._datasource.CreateLayer( self._dtName, srs, ogr.wkbPoint)
        if self._dataset is None:
            self._logger.error ("Failed to create dataset %s \n" % self._dtName)
            sys.exit(1)
        if toFormat == "PostgreSQL":
            self._datasource.ExecuteSQL("update geometry_columns set srid=%d where f_table_name = '%s';" %(epsgCode,self._dtName))
        
        filePoint = open(fileName,'r')
        csvReader = csv.reader(filePoint,delimiter = " ")
        
        firstRow = True
        for line in csvReader:
            if firstRow is True:
                firstRow = False
                nCols = len(line)
                for i in range(0, nCols - 2 ):
                    field_defn = ogr.FieldDefn( "Z%d"%i, ogr.OFTReal )
                    if self._dataset.CreateField ( field_defn ) != 0:
                        self._logger.error ("Failed to create field Z%d  for dataset %s /n" %(i,self._dtName))
                        sys.exit(1)
                     
            x = float(line[0])
            y = float(line[1])
            feat = ogr.Feature(self._dataset.GetLayerDefn())
            for i in range(0, nCols - 2 ):
                feat.SetField( "Z%d"%i, float(line[2+i]))
            pt = ogr.Geometry(ogr.wkbPoint)
            pt.SetPoint_2D(0, x, y)    
            feat.SetGeometry(pt)     
            if self._dataset.CreateFeature(feat) != 0:
                self._logger.error ("Failed to create feature for dataset %s" %self._dtName)
                sys.exit(1)
            feat.Destroy()   
        #refresh the
        msg = "Finished importing vector file: %s \n" %fileName
        self._logger.info(msg)
        self.refresh() 
        
       # srs = self._dataset.GetSpatialRef()
       # srs.ExportToWkt(self._projection)
        
       # print projection
       # print srs.ExportToWkt()
      
        
    def refresh(self):
        
        self._datasource =None
        self._dataset = None
        self._datasource = ogr.Open(self._dsName )
        self._dataset = self._datasource.GetLayer(self._dtName)
        
    def getBounds(self):
        
        if not self._dataset is None:
            self._bounds =self._dataset.GetExtent()  
        return self._bounds
    
    def getMin(self,field):
        """
         OGR SQL doesn't work as expected, so a naive way is resorted.
        """
        minValue = 9999999999999
        #if not self._datasource is None:
            #sql = "(select * from \"%s\" where %s = (select min(%s) from \"%s\"));" %(self._dtName,field,field,self._dtName)
            #minValue = self._datasource.ExecuteSQL(sql);
            #feat = minValue.GetFeatureCount()
            #index = feat.GetFieldIndex(field)
            #test = feat.GetFieldAsDouble(1)
        #return minValue
        if not self._dataset is None:
            featureCount = self._dataset.GetFeatureCount()
            for i in range(1,featureCount):
                feature = self._dataset.GetFeature(i)
                fieldIndex = feature.GetFieldIndex(field)
                if fieldIndex >=0:
                    fieldValue = feature.GetFieldAsDouble(fieldIndex)
                    if fieldValue < minValue:
                        minValue = fieldValue
                    
        
    def getMax(self,field):
        """
         OGR SQL doesn't work as expected, so a naive way is resorted.
        """
      
        maxValue = -9999999999999
        #if not self._datasource is None:
            #sql = "(select * from \"%s\" where %s = (select min(%s) from \"%s\"));" %(self._dtName,field,field,self._dtName)
            #minValue = self._datasource.ExecuteSQL(sql);
            #feat = minValue.GetFeatureCount()
            #index = feat.GetFieldIndex(field)
            #test = feat.GetFieldAsDouble(1)
        #return minValue
        if not self._dataset is None:
            featureCount = self._dataset.GetFeatureCount()
            for i in range(1,featureCount):
                feature = self._dataset.GetFeature(i)
                fieldIndex = feature.GetFieldIndex(field)
                if fieldIndex >=0:
                    fieldValue = feature.GetFieldAsDouble(fieldIndex)
                    if fieldValue > maxValue:
                        maxValue = fieldValue
                    
    def getExtremes(self,field):
        """
         Put min and max in a same loop to save access of database
        """
        minValue = 999999999
        maxValue = -minValue
        #if not self._datasource is None:
        #    sql = "SELECT MIN(%s) FROM %s;" %(field,self._dtName)
        #    subset = self._datasource.ExecuteSQL(sql)
        #    count = subset.GetFeatureCount()
        #    if count > 0:
        #        feature = subset.GetFeature(0)
        #        minValue = feature.GetFieldAsDouble(0)
        #    
        #    sql = "SELECT MAX(%s) FROM %s;" %(field,self._dtName)
        #    subset = self._datasource.ExecuteSQL(sql)
        #    count = subset.GetFeatureCount()
        #    if count > 0:
        #        feature = subset.GetFeature(0)
        #       maxValue = feature.GetFieldAsDouble(0)
            
        #return minValue
        if not self._dataset is None:
            featureCount = self._dataset.GetFeatureCount()
            for i in range(0,featureCount):
                feature = self._dataset.GetFeature(i)
                fieldIndex = feature.GetFieldIndex(field)
                if fieldIndex >=0:
                    fieldValue = feature.GetFieldAsDouble(fieldIndex)
                    if fieldValue < minValue:
                        minValue = fieldValue
                    if fieldValue > maxValue:
                        maxValue = fieldValue
                       
        return minValue, maxValue
                    
                    
    def getBins(self, fieldName,N=10):
        """Get N values between the min and the max occurred in this dataset.
        """     
        minValue, maxValue = self.getExtremes(fieldName)
        
        levels = []
        # Linear intervals
        d = (maxValue - minValue)/N    
        for i in range(N):
                levels.append(minValue+i*d)            
        return levels
    
    def getFieldCount(self):
        """
        get the number of fields
        """
        if self._dataset is None:
            return None
        fieldCount = None
        featDefn = self._dataset.GetLayerDefn()
        if not featDefn is None:
            fieldCount= featDefn.GetFieldCount()
        return fieldCount 
    
    def getFieldIndex (self,fieldName):
        fieldIndex = -1
        if self._dataset is None or fieldName is None:
            return fieldIndex
        featDefn = self._dataset.GetLayerDefn()
        if not featDefn is None:
            fieldIndex= featDefn.GetFieldIndex(fieldName)
        return fieldIndex

if __name__ == "__main__":
    config = VizConfiguration.getInstance()
    dsName = 'PG:dbname=%s user=%s password=%s port=%s'%(config.getVectorDBName(),config.getVectorDBUserName(),config.getVectorDBPassword(),config.getVectorDBPort())
    
    dtName = '1311_vector'
    vectorDataset = OgrVectorDataset(dsName,dtName) 
    print vectorDataset.getExtremes('z0')
    #vectorDataset.importFromFile("/var/tmp/results/results/output.xy","/var/tmp/results/results/26915.prj")
    #vectorDataset.getBounds()
     
