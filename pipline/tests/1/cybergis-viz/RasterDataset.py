from Dataset import Dataset
import osgeo.gdal as gdal
import osgeo.osr as osr
import os
import gdalconst
import locale

class RasterDataset(Dataset):
    """
       base class for raster dataset (file based, or database-based dataset)
       @author:  Guofeng Cao
    """
    Unknown = 0
    Byte = 1
    UInt16 = 2
    Int16 = 3
    UInt32 = 4 
    Int32 = 5
    Float32 = 6
    Float64 = 7  
    
    def __init__(self, dsName = None, dtName = None):
        dsName = dsName.encode(locale.getpreferredencoding())
        Dataset.__init__(self, dsName, dtName)
        self._type = Dataset.RASTER
        self._override_colortable=True
    def open(self, dsName = None, dtName = None):
        Dataset.open(self, dsName, dtName)
    def setOverrideColorTable(self, bOverride=True):
        self._override_colortable=bOverride
    def getOverrideColorTable(self):
        return self._override_colortable
    def close(self):
        Dataset.close(self)
    def getRasterBandCount(self):
        raise NotImplementedError
    def getPixelSizeX(self):
        raise NotImplementedError
    def getPixelSizeY(self):
        raise NotImplementedError
    def getOriginX(self):
        raise NotImplementedError
    def getOriginY(self):
        raise NotImplementedError
    def getXSize(self):
        raise NotImplementedError
    def getYSize(self):
        raise NotImplementedError
    def getNoDataValue(self):
        raise NotImplementedError
    def getDataType(self):
        raise NotImplementedError

class FileRasterDataset(RasterDataset):
    """
        file raster dataset
        @note: dtName here is just a place holder for database based dataset
        @author:  Guofeng Cao
    """
    def __init__(self,dsName=None, dtName=None):
        #super(FileRasterDataset, self).__init__()
        RasterDataset.__init__(self,dsName,dtName)
        if not dsName is None:
            self.open(dsName, dtName)
        
    def open(self,dsName=None, dtName=None):
        # if already open, return
        if not self._datasource is None or not self._dataset is None:
            return  
        if  self._dsName is None:
            return
        pathname, extension = os.path.splitext(self._dsName)
        self._dtName = os.path.split(pathname)[-1] 
        gdal.AllRegister()
        self._dataset = gdal.Open(self._dsName, gdalconst.GA_ReadOnly)
        if self._dataset is None:
            self._logger.error("Failed to open file %s", self._dsName)
            raise "Failed to open raster data file"    
    def getProjection (self):
        projection = None
        if not self._dataset is None:
            projection= self._dataset.GetProjection()
        return projection
        
    def setProjection(self,projection):
        if not self._dataset is None:
            self._dataset.SetProjection(projection)
    
    def setProjectionByEpsgCode(self,code):
        """
            set projection information by epsg code, and serialized in the file, so refresh is needed
        """
        if not self._dataset is None:
            srs = osr.SpatialReference()
            srs.ImportFromEPSG(code) 
            wkt = srs.ExportToWkt()
            # reopen the dataset with writable mode
            self._dataset = None
            self._dataset = gdal.Open(self._dsName, gdalconst.GA_Update)
            self._dataset.SetProjection(wkt)
            self.refresh()
            
    def refresh(self): # reopen 
        if not self._dataset is None:
            self._dataset = None
            self._dataset = gdal.Open(self._dsName, gdalconst.GA_ReadOnly)
             
    def getMax(self):
        if self.getRasterBandCount()>0:
            band = self._dataset.GetRasterBand(1)
            if band is None:
                self._logger.error("The band of %s is empty", self._dsName)    
                return -1
            
            maxValue= band.GetMaximum()
            if maxValue is None:
                (minValue,maxValue)=band.ComputeRasterMinMax(1)
            return maxValue
        
         
    def getMin(self):
        if self.getRasterBandCount()>0:
            band = self._dataset.GetRasterBand(1)
            if band is None:
                self._logger.error("The band of %s is empty", self._dsName)    
                return -1
            
            minValue= band.GetMinimum()
            if minValue is None:
                (minValue,maxValue)=band.ComputeRasterMinMax(1)
            return minValue
                  
         
    def getBounds(self): # bounds in gdal format 
        geoTransform = self._dataset.GetGeoTransform()
        if geoTransform is None:
            self._logger.error("Failed to get information of file: ", self._dsName)
            return None
        self._bounds = [0,0,0,0]
        self._bounds[0]= geoTransform[0]
        self._bounds[1]= geoTransform[3]
        self._bounds[2]= geoTransform[0]+geoTransform[1]*self.getXSize()
        self._bounds[3]= geoTransform[3]+geoTransform[5]*self.getYSize()
        return self._bounds
        
    def query(self, bb,filter):
        return None 
        
    def getRasterBandCount(self):
        return self._dataset.RasterCount
    
    def getPixelSizeX(self):
        geoTransform = self._dataset.GetGeoTransform()
        if geoTransform is None:
            self._logger.error("Failed to get information of file: ", self._dsName)
            return None
        return geoTransform[1]
 
    def getPixelSizeY(self):
        geoTransform = self._dataset.GetGeoTransform()
        if geoTransform is None:
            self._logger.error("Failed to get information of file: ", self._dsName)
            return None
        return geoTransform[5]
    
    def getOriginX(self): #get top left x 
        geoTransform = self._dataset.GetGeoTransform()
        if geoTransform is None:
            self._logger.error("Failed to get information of file: ", self._dsName)
            return None
        return geoTransform[0]
     
    def getOriginY(self): # get top left y
        geoTransform = self._dataset.GetGeoTransform()
        if geoTransform is None:
            self._logger.error("Failed to get information of file: ", self._dsName)
            return None
        return geoTransform[3]

    def getXSize(self):
        return self._dataset.RasterXSize
    def getYSize(self):
        return self._dataset.RasterYSize
    def getDataType(self):
        dataType = RasterDataset.Unknown
        if self.getRasterBandCount()>0:
            band = self._dataset.GetRasterBand(1)
            if band is None:
                self._logger.error("The band of %s is empty", self._dsName)    
                return None
            dataType = band.DataType
            if dataType is gdalconst.GDT_Byte:
                dataType = RasterDataset.Byte
            if dataType is gdalconst.GDT_UInt16:
                dataType = RasterDataset.UInt16
            if dataType is gdalconst.GDT_Int16:
                dataType = RasterDataset.Int16
            if dataType is gdalconst.GDT_UInt32:
                dataType = RasterDataset.UInt32
            if dataType is gdalconst.GDT_Int32:
                dataType = RasterDataset.Int32
            if dataType is gdalconst.GDT_Int32:
                dataType = RasterDataset.Int32
            if dataType is gdalconst.GDT_Float32:
                dataType = RasterDataset.Float32
            if dataType is gdalconst.GDT_Float64:
                dataType = RasterDataset.Float64
        return dataType
        
    def getNoDataValue(self):
        nodata = None
        if self.getRasterBandCount()>0:
            band = self._dataset.GetRasterBand(1)
            if band is None:
                self._logger.error("The band of %s is empty", self._dsName)    
                return None
            nodata= band.GetNoDataValue()
        if nodata is None:
            nodata = self.getMin()          
        return nodata
    def getBins(self, N=10):
        """Get N values between the min and the max occurred in this dataset.
        
        """     
        minValue = self.getMin()
        maxValue = self.getMax() 

        levels = []
        # Linear intervals
        d = float((maxValue - minValue)*1.0/N)    
        for i in range(N):
                levels.append(minValue+i*d)            
        return levels

