from Singleton import Singleton
import os,logging
from VizConfiguration import VizConfiguration
from Dataset import Dataset
from RasterDataset import FileRasterDataset
from Utilities import get_web_page, curl, run
import pycurl
import StringIO
import json, re
from os.path import basename
import sys

class GeoserverConnector (Singleton):
    """
    A class to  connect the Geoserver via Curl 
    @author Guofeng Cao
    @todo: 1,make it an abstract class so that it could account for future python interface of geoserver, which seems not very mature for now.
    @todo: 2,make it multi-threading
    """
    def __init__(self):
        super(GeoserverConnector, self).__init__()   
        
        config = VizConfiguration.getInstance()
        self._logger = logging.getLogger(config.getAppName())
        
        self._geoserverHost = "http://" + config.getGeoserverHost() + ":"+ config.getGeoserverPort() + "/geoserver"
        self._geoserverUserName = config.getGeoserverUserName()
        self._geoserverPassword = config.getGeoserverPassword()
        self._workspace = config.getGeoserverNamespace()
        self._sldPath= config.getSLDPath()
        
        self._vectorDBHost = config.getVectorDBHost()
        self._vectorDBPort = config.getVectorDBPort()
        self._vectorDBUserName = config.getVectorDBUserName()
        self._vectorDBName = config.getVectorDBName()
        self._vectorDBPassword = config.getVectorDBPassword()
        
        # direction for the working point dataset
        self._tempPath= None
        
        
        # Verify connection
        found = False 
        page = get_web_page(os.path.join(self._geoserverHost, 'rest'), 
                            username=self._geoserverUserName, 
                            password=self._geoserverPassword)
        for line in page:
            if line.find('workspaces') > 0:
                found = True
                
        if found == True:
            self._logger.info("Successfully connected to geoserver: %s \n"%self._geoserverHost)
        else:
            self._logger.error("Fail to connect to geoserver: %s \n"%self._geoserverHost)
        
           
        found = False
        page = get_web_page(os.path.join(self._geoserverHost, 'rest/workspaces/'), 
                            username=self._geoserverUserName, 
                            password=self._geoserverPassword)
        for line in page:
            if line.find("/workspaces/%s.html"%self._workspace) > 0:
                found = True
        
        if found == True:
            self._logger.info("Successfully connected to geoserver: %s \n"%self._geoserverHost)
        else:
            self._logger.error("Fail to connect to geoserver: %s \n"%self._geoserverHost)

        self.createWorkspace(self._workspace)

                    
        return
    
    def getWMSPrefix(self):
        
        config = VizConfiguration.getInstance()
        wmsPrefix = "http://" + config.getGeoserverHost() + ":" + config.getGeoserverPort() + "/" + "geoserver" + "/" + "wms?"
        return wmsPrefix
    
    def getGWCPrefix(self):
        """
          send requests to geowebcache server
        """
        config = VizConfiguration.getInstance()
        wmsPrefix = "http://" + config.getGeoserverHost() + ":" + config.getGeoserverPort() + "/" + "geoserver" + "/" +"gwc" + "/" + "service" + "/" + "wms?"
        return wmsPrefix
        
    def setTempPath(self,path):
        self._tempPath = path
        
            
    # Methods for manipulating the geoserver (e.g. add and delete workspaces)
    def createWorkspace(self, name, verbose=False):
        """
            Create new workspace on the geoserver Generate and execute curl commands of the form
            curl -u admin:geoserver -v -X POST -H "Content-type: text/xml" \
             "http://localhost:8080/geoserver/rest/workspaces" --data-ascii \
             "<workspace><name>cigi</name></workspace>"
        """
        found = False
        page = get_web_page(os.path.join(self._geoserverHost, 'rest/workspaces/'),
                            username=self._geoserverUserName,
                            password=self._geoserverPassword)
        for line in page:
            if line.find("/workspaces/%s.html"%name) > 0:
                found = True

        if found==True:
            self._logger.info("Workspace %s exists \n"%name)
        else:
            self._logger.info("Workspace %s doesn't exist, trying to create it  \n"%name)
            #self.createWorkspace(self._workspace)
            self._logger.info("Creating workspace at %s@%s \n"%(self._geoserverUserName,self._geoserverHost))
            curl(self._geoserverHost,
                 self._geoserverUserName,
                 self._geoserverPassword,
                 'POST',
                 'text/xml',
                 'workspaces',
                 '--data-ascii',
                 '<workspace><name>%s</name></workspace>' % name,
                 verbose=verbose,logger = self._logger)
        self._workspace=name

        
        
    def getWorkspace(self, name, verbose=False):
        """Get workspace info from the geoserver
        """

        out = curl(self._geoserverHost, 
             self._geoserverUserName, 
             self._geoserverPassword, 
             'GET', 
             'text/xml', 
             'workspaces/%s' % name, 
             '', 
             '', 
             verbose=verbose,logger = self._logger)
        
    def createCoverageStore(self,workspace,store,verbose=False):
        
        curl(self._geoserverHost, 
             self._geoserverUserName, 
             self._geoserverPassword, 
             'POST', 
             'text/xml', 
             'workspaces/%s/coveragestores/' % workspace, 
             '--data-ascii', 
             '<coveragestore><name>%s</name><workspace>%s</workspace><enabled>true</enabled></coveragestore>' % (store,workspace), 
             verbose=verbose,logger = self._logger)
    
    def checkLayer(self, layerName):
        """Check if the layer is in the workspace"""
        found = False
        page = get_web_page(os.path.join(self._geoserverHost, 'rest/layers/'),
                            username=self._geoserverUserName,
                            password=self._geoserverPassword)
        for line in page:
            if line.find("/layers/%s.html"%layerName) > 0:
                found = True

        return found

    # fieldName is only for vector dataset
    def addLayer(self, dataset, layerName=None, fieldName = None, apptype="viewshed"):
        """
        add layer to the geoserver, it handles the types of the input dataset automatically.
        """
        if dataset is None:
            return
        if layerName is None:
            layerName = dataset.getDtName()
        found = self.checkLayer(layerName)
        if found == True:
            self._logger.info("Warning: layer {} found, stop adding layer".format(layerName))
            return False
        if apptype == "flukde":
            self.removeCachedLayer(self._workspace, layerName)
#        if apptype == "taudem":
#            self.removeCachedLayer('TauDEM', layerName)
        if dataset.getType()== Dataset.RASTER:
            self._addRasterLayer(dataset,self._workspace,layerName,False,apptype)
        if dataset.getType()== Dataset.POINT or dataset.getType()== Dataset.LINE or dataset.getType()== Dataset.POLYGON:
            self._logger.info("===>Adding vector layer " + layerName + " field " + fieldName + "...")
            self._addVectorLayer(dataset,fieldName,self._workspace,layerName,False, apptype)
        return True

    # remove layer from Geowebcache before adding
    def removeCachedLayer(self,workspace,layerName):
        if layerName is None:
            return    
        #curl -v -u admin:cgtest -XDELETE "http://sandbox-viz-flu.cigi.illinois.edu:8080/geoserver/gwc/rest/layers/medford.xml"
        #print "curl -u %s:%s -XDELETE %s/gwc/rest/layers/%s:%s.xml" % (self._geoserverUserName, self._geoserverPassword, self._geoserverHost, workspace, layername)

        run("curl -u %s:%s -XDELETE %s/gwc/rest/layers/%s:%s.xml" % (self._geoserverUserName, 
            self._geoserverPassword, self._geoserverHost, workspace, layerName), verbose= True,logger = self._logger)        
     
    def _addRasterLayer(self, dataset,workspace, layerName=None, verbose=False, apptype="viewshed", style_filename=None):
        """
        Uploads are done using curl commands of the form
        curl -u admin:geoserver -v -X PUT -H "Content-type: image/tif" "http://localhost:8080/geoserver/rest/workspaces/futnuh/coveragestores/population_padang_1/file.geotiff" --data-binary "@data/population_padang_1.tif
        """
        # only support file database for now
        if not isinstance(dataset,FileRasterDataset):
            return;
        #print dataset.getProjection()
        filename = dataset.getDsName()
        pathname, extension = os.path.splitext(filename)
        if layerName is None:
            layerName = os.path.split(pathname)[-1]
        
        msg = 'Coverage must have extension tif'
        assert extension is not '.tif', msg

        # Style file in case it accompanies the file        
        #style_filename = pathname + '.sld'
        
        
        # Upload raster data to Geoserver
        curl(self._geoserverHost, 
             self._geoserverUserName, 
             self._geoserverPassword, 
             'PUT', 
             'image/tif', 
             'workspaces/%s/coveragestores/%s/file.geotiff' % (workspace, layerName), 
             '--data-binary', 
             '@%s'%filename, 
             verbose=verbose, logger=self._logger)
        
        if dataset.getOverrideColorTable()==False:
            return

        if apptype is not "flukde":            
            # Style file in case it accompanies the file        
            #style_filename = pathname + '.sld'
            if style_filename is None:
                style_filename = self._tempPath + "/" + layerName +'.sld'
            
            # Take care of styling 
            if not os.path.isfile(style_filename):
                self.createSimpleRasterSLD(dataset,layerName, verbose, apptype)
     
            # Upload style file to Geoserver    
            self.addStyle(layerName, style_filename, verbose)            
                
            # Make it the default for this layer
            self.setDefaultStyle(layerName, layerName, verbose)


    def _addVectorLayer(self, dataset, fieldName,workspace, layerName=None, verbose=False,apptype="viewshed"):
        #layerName = dataset.GetName()
        if dataset is None:
            return
        
        if dataset.getFileType() == 1:
            xml = "<dataStore><name>%s</name> <connectionParameters><host>%s</host><port>%s</port><database>%s</database><user>%s</user><passwd>%s</passwd><dbtype>postgis</dbtype></connectionParameters></dataStore>"%(self._vectorDBName,self._vectorDBHost,self._vectorDBPort,self._vectorDBName,self._vectorDBUserName,self._vectorDBPassword)
                    
            curl(self._geoserverHost, 
                     self._geoserverUserName, 
                 self._geoserverPassword, 
                 'POST', 
                 'text/xml', 
                 'workspaces/%s/datastores/' % (workspace), 
                 '--data-ascii',
                 xml, 
                 verbose=verbose,logger = self._logger)
            
            dtName = dataset.getDtName()
            if layerName is None:
                layerName = dtName
                
            curl(self._geoserverHost, 
                 self._geoserverUserName, 
                 self._geoserverPassword, 
                 'POST', 
                 'text/xml', 
                 'workspaces/%s/datastores/%s/featuretypes' % (workspace,self._vectorDBName), 
                 '--data-ascii',
                 '<featureType><name>%s</name><title>%s</title></featureType>'%(dtName,dtName) , 
                 verbose=verbose,logger = self._logger)
            
        else:
            fileName = dataset.getDsName()
            subdir = os.path.split(fileName)[0]
            local_filename = os.path.split(fileName)[1]
            upload_filename = subdir + '/' + layerName + '.zip'
            cmd = 'cd %s; zip %s %s*' % (subdir, upload_filename, layerName)
            self._logger.info("Vector layer dataset zip: " + cmd)
            run(cmd, None, None, verbose=verbose)
            # Move to cwd
            # FIXME (Ole): For some reason geoserver won't accept the zip file unless
            # located in CWD.       
            # FIXME (Ole): If zip file already exists with different owner, this
            # will silently wait for a newline. Annoying.     
            #cmd = 'mv %s/%s .' % (subdir, upload_filename)
            #run(cmd, stdout='mvzip.stdout', stderr='mvzip.stderr', verbose=verbose)
            
                    # Upload vectore data to Geoserver       
            # curl(self._geoserverHost, 
            #       self._geoserverUserName, 
            #       self._geoserverPassword, 
            #       'PUT', 
            #       'text/plain', 
            #       'workspaces/%s/datastores/%s/file.shp' % (workspace,layerName), 
            #       '--data-binary',
            #       '@%s' % upload_filename, 
            #       verbose=verbose,logger = self._logger)
            #curl -u admin:geoserver -v -XPUT -H 'Content-type: application/zip' --data-binary @/var/tmp/cigi/4468/results/4468_randpts.zip http://localhost:8080/geoserver/rest/workspaces/cigi/datastores/4468_randpts/file.shp
            
            # the curl function doen't succeed due to a geoserver bug, adhoc call has to be made. 
            cmd = "curl -u %s:%s -v -XPUT -H 'Content-type: application/zip' --data-binary @%s %s/rest/workspaces/%s/datastores/%s/file.shp" %(self._geoserverUserName,self._geoserverPassword, upload_filename,self._geoserverHost, workspace, layerName)
            self._logger.info("Vector layer REST call: " + cmd)
            run(cmd, None, None, verbose=verbose)             
                
        style_filename =  self._tempPath + "/" + layerName +'.sld'
        self._logger.info("Looking for style file: " + style_filename)
        
         # Take care of styling 
        if not os.path.isfile(style_filename):
            if dataset.getType() == Dataset.POINT:
                self.createSimplePointSLD(dataset, fieldName, layerName, verbose=verbose,apptype=apptype)
            else:
                return
 
        # Upload style file to Geoserver    
        self.addStyle(layerName, style_filename, verbose=verbose)            
            
        # Make it the default for this layer
        self.setDefaultStyle(layerName, layerName, verbose=verbose)

    def addImagePyramid(self, name, path, workspace, verbose=False):
        self.addImagePyramidCoverage(name, workspace, verbose)
        self.addImagePyramidLayer(name, path, workspace, verbose)
        self.addImagePyramidLayer(name, path, workspace, verbose)

    def addImagePyramidCoverage(self, name, workspace, verbose=False):
        #        curl -u admin:cgadmin -v -XPOST -H "Content-Type: application/xml" -d "<coverageStore><name>ned_apitest</name><workspace>cigi</workspace><enabled>true</enabled></coverageStore>" http://localhost:8080/geoserver/rest/workspaces/cigi/coveragestores
#

        #sys.stdout.write(name + "," + path + "," + workspace + "\n")

        coverageStoreXML = "<coverageStore><name>%s</name><workspace>%s</workspace><enabled>true</enabled></coverageStore>" % (name, workspace)
        curl(self._geoserverHost, 
                self._geoserverUserName, 
                self._geoserverPassword, 
                'POST', 
                'application/xml', 
                'workspaces/%s/coveragestores/' % (workspace), 
                '--data-ascii',
                coverageStoreXML, 
                verbose=verbose,logger = self._logger)

    def addImagePyramidLayer(self, name, path, workspace, verbose=False, style_filename=None):
         #       curl -u admin:cgadmin -v -XPUT -H "Content-type: text/plain" -d"file:///opt/dem/nedpyramid_apitest" http://localhost:8080/geoserver/rest/workspaces/cigi/coveragestores/ned_apitest/external.imagepyramid
        #path = "file://" + path 
        #sys.stdout.write(self._geoserverHost+","+self._geoserverUserName+","+self._geoserverPassword+","+coverageStoreXML+"\n");
        curl(self._geoserverHost, 
                self._geoserverUserName, 
                self._geoserverPassword, 
                'PUT', 
                'text/plain', 
                'workspaces/%s/coveragestores/%s/external.imagepyramid' % (workspace, name), 
                '--data-ascii',
                path, 
                verbose=verbose,logger = self._logger)
	# add style
        if style_filename is not None:
            self.addStyle(name, style_filename, verbose=verbose)
            self.setDefaultStyle(name, name, verbose=verbose)

    def removeLayer(self, layerName):
        
        # TODO: Add tests
        run("curl -u %s:%s -XDELETE localhost:8080/geoserver/rest/layers/%s" % (self._geoserverUserName, 
            self._geoserverUserName, 
            layerName),logger = self._logger)
            
        
    def findStyle(self, name):
        """Does the style exist"""
        c = pycurl.Curl()
        url = ((self.geoserver_url+"/rest/styles/%s") % name)
        c.setopt(pycurl.URL, url)
        c.setopt(pycurl.HTTPHEADER, ["Accept: text/json"])
        c.setopt(pycurl.USERPWD, "%s:%s" % (self.geoserver_username, self.geoserver_userpass))
        c.setopt(pycurl.VERBOSE, 0)
        b = StringIO.StringIO()
        c.setopt(pycurl.WRITEFUNCTION, b.write)
        c.perform()
        m = re.match("No such style: ", b.getvalue())
        if m: return None
        d = json.loads(b.getvalue())
        return d

    def createSimpleRasterSLD(self, dataset, layername=None,verbose=False,apptype="viewshed"):
        
        """given a raster file and a predefined SLD template it should find the min,max,nodata values
        for the dataset and create a custom style with a color map using the predefined template sld
        which is located at: ./sld_templates/raster-sld-template.xml
        
        @todo:  SLD should be encapusalted as a single class as the complexity increases in the future
        @see: SLDGenerator 
        
        """

        #sld_template = get_pathname_from_package('api')+'/sld_templates/sld_template.xml'

        filename = dataset.getDsName()
        pathname, extension = os.path.splitext(filename)
        
        if layername is None:
            layername = dataset.getDtName()
            
        levels = dataset.getBins(N=10)
        nodata = dataset.getNoDataValue()
        
        sld_template = self._sldPath + "/raster-sld-template.xml"
        
        if apptype=="flukde":
            sld_template = self._sldPath + "/raster-sld-template-flu.xml" 
         #   self._logger.error("test:flukde")
        
        #self._logger.error(apps)
        dataType = dataset.getDataType()
        if dataType == FileRasterDataset.Byte:
            sld_template = self._sldPath + "/raster-sld-template-distinct.xml"
            
        if not os.path.isfile(sld_template):
            msg = 'Could not find sld template file: %s \n' %sld_template
            self._logger.error(msg)
            raise Exception(msg)

        #if verbose:
        #    print 'Styling %s' %layername        
        #    print 'Levels', levels
        #    print 'NoData', nodata    
        
        # Write the SLD file    
        
        
        sld = self._tempPath + "/" + layername +'.sld'
         
        f = open(sld_template, 'r')
        text = f.read()
        
        if dataType is not FileRasterDataset.Byte:
            text = text.replace('MIN',str(round(levels[0],3)))
            text = text.replace('MAX',str(round(levels[-1],3)))
            text = text.replace('TEN',str(round(levels[1],3)))
            text = text.replace('TWENTY',str(round(levels[2],3)))
            text = text.replace('THIRTY',str(round(levels[3],3)))
            text = text.replace('FOURTY',str(round(levels[4],3)))
            text = text.replace('FIFTY',str(round(levels[5],3)))
            text = text.replace('SIXTY',str(round(levels[6],3)))
            text = text.replace('SEVENTY',str(round(levels[7],3)))
            text = text.replace('EIGHTY',str(round(levels[8],3)))
            text = text.replace('NINETY',str(round(levels[9],3)))

        # Getting round an sld parsing bug in geoserver
        if nodata >= max:
            text = text.replace('<!--Higher-->', '<ColorMapEntry color="#ffffff" quantity="NODATA" label="NoData: NODATA" opacity="0"/>')
        else:   
            text = text.replace('<!--Lower-->', '<ColorMapEntry color="#ffffff" quantity="NODATA" label="NoData: NODATA" opacity="0"/>')
        
        text = text.replace('NODATA', str(nodata))
        
        fout = open(sld, 'w')
        fout.write(text)
        fout.close()
    
    def createSimplePointSLD(self, dataset, fieldName,layername = None, verbose=False,apptype="viewshed"):
        
        if dataset is None:
            return
        if layername is None: 
            layername = dataset.getDtName()
            
        sld = self._tempPath + "/" + layername +'.sld'
        
        # if field does't exist, just copy the template file
        if dataset.getFieldIndex(fieldName) < 0:
            sld_template = self._sldPath + "/point-sld-template-no-field.xml"        
            if not os.path.isfile(sld_template):
                msg = 'Could not find sld template file: %s \n' %sld_template
                self._logger.error(msg)
                raise Exception(msg)
            
            strcmd = 'cp %s %s' %(sld_template, sld)
            run(strcmd, verbose=False, logger = self._logger)
            return
            
        # otherwise, generate the sld file according to the field
        
        sld_template = self._sldPath + "/point-sld-template.xml"        
        if not os.path.isfile(sld_template):
            msg = 'Could not find: '+sld_template
            raise Exception(msg)
        
        levels = dataset.getBins(fieldName,N=10)
        f = open(sld_template, 'r')
        text = f.read()

        text = text.replace('SamplePropertyName',fieldName)
        text = text.replace('MIN',str(levels[0]))
        text = text.replace('MAX',str(levels[-1]))
        text = text.replace('TEN',str(levels[1]))
        text = text.replace('TWENTY',str(levels[2]))
        text = text.replace('THIRTY',str(levels[3]))
        text = text.replace('FOURTY',str(levels[4]))
        text = text.replace('FIFTY',str(levels[5]))
        text = text.replace('SIXTY',str(levels[6]))
        text = text.replace('SEVENTY',str(levels[7]))
        text = text.replace('EIGHTY',str(levels[8]))
        text = text.replace('NINETY',str(levels[9]))


        # Getting round an sld parsing bug in geoserver
            
        fout = open(sld, 'w')
        fout.write(text)
        fout.close()
        
    
    
    def addStyle(self, style_name, style_file, verbose=False):
        """Upload style file to geoserver
        """     
           
        # curl -u geoserver -XPOST -H 'Content-type: text/xml' -d 
        # '<style><name>sld_for_Pk50095_geotif_style</name><filename>Pk50095.sld</filename></style>' 
        # localhost:8080/geoserver/rest/styles/ 
        curl(self._geoserverHost, 
             self._geoserverUserName, 
             self._geoserverPassword, 
             'POST', 
             'text/xml', 
             'styles', 
             '--data-ascii', 
             '<style><name>%s</name><filename>%s</filename></style>' % (style_name, basename(style_file)),  
             verbose=verbose, logger = self._logger)
        

        # curl -u geoserver -XPUT -H 'Content-type: application/vnd.ogc.sld+xml' -d @sld_for_Pk50095_geotif.sld
        # localhost:8080/geoserver/rest/styles/sld_for_Pk50095_geotif_style
        curl(self._geoserverHost, 
            self._geoserverUserName, 
            self._geoserverPassword, 
            'PUT', 
            'application/vnd.ogc.sld+xml', 
            'styles/%s' % (style_name), 
            '--data-binary', 
            '@%s' % style_file, 
            verbose=verbose,logger = self._logger)

    def removeStyle(self, style_name):
        """docstring for delete_style"""
        # TODO: add test
        run("curl -u %s:%s -d 'purge=true' -XDELETE localhost:8080/geoserver/rest/styles/%s" % (self._geoserverUserName, 
            self._geoserverPassword, 
            style_name), logger = self._logger)
        
    def setDefaultStyle(self, style_name, layer_name, verbose=False):
        """Set given style as default for specified layer"""
        
        curl(self._geoserverHost, 
            self._geoserverUserName, 
            self._geoserverPassword, 
            'PUT', 
            'text/xml', 
            'layers/%s' % (layer_name), 
            '--data-ascii', 
            '<layer><defaultStyle><name>%s</name></defaultStyle><enabled>true</enabled></layer>' % style_name,
            verbose=verbose,logger = self._logger)
        
        # curl -u admin:geoserver -XPUT -H 'Content-type: text/xml' -d 
        # '<layer><defaultStyle><name>Pk50095_geotif_style</name></defaultStyle><enabled>true</enabled></layer>' 
        # localhost:8080/geoserver/rest/layers/Pk50095_geotif
    
    
#if __name__ == "__main__":
#    test = GeoserverConnector.getInstance()
#    test.removeCachedLayer("world","volcanoes")
         
    
    
class SLDGenerator(Singleton):
    """
    A class to  generate sld file for dataset
    @author Guofeng Cao
    @todo: make it more specific for raster and vector dataset to account for more cartography needs
    """
    RAMP = 1
    INTERVAL =2
    def __init__(self):
        super(SLDGenerator, self).__init__()
    def getSld (self):
        pass 
        
        
    
