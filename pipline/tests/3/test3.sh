module load python/2.7.10
module load gdal-stack
files=`python mosaic.py /gpfs_scratch/usgs/ned10m/ned10m.geojson /gpfs_scratch/ned_data_boundaries/State/geojson/MA.geojson`
echo $files
gdal_merge.py -o mosaic.tif $files
echo mosaic
gdalwarp -cutline /gpfs_scratch/ned_data_boundaries/State/geojson/MA.geojson -crop_to_cutline -of VRT            mosaic.tif MA.vrt
gdalwarp -t_srs "EPSG:26957" -of VRT MA.vrt MA_repro.vrt
gdalwarp MA_repro.vrt MA.tif
gdaldem hillshade MA.tif MA_hillshade.tif
python cybergis-viz/VizMain.py MA_hillshade.tif
rm MA_hillshade.tif
rm MA.vrt MA_repro.vrt MA.tif
rm mosaic.tif
