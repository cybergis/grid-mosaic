#!/usr/bin/env/ python
import fiona
import argparse
import subprocess
from shapely.geometry import shape


def createMosaic(tileIndex, aoi):
    """
    Returns a virtual raster made up of specified tiles.

    The input area of interest vector is overlaid against the grid. 
    Then the tiles that are in the intersection of the two are returned 
    as a virtual raster. 

    Parameters
    ----------
    tileIndex:  - Grid geoJSON
    aoi:        - Input area of interest vector to overlap against grid
    output:     - Name of the output virtual raster

    """
    filename = ""
    for layername in fiona.listlayers(aoi):
        with fiona.open(aoi, layer=layername) as source:
            with fiona.open(tileIndex) as grid:
                # Process all the records in the input vector
                for f in source:
                    # Shapely's shape function to turn a dictionary object
                    # into a Shapely Polygon
                    geom = shape(f['geometry'])
                    # Process all the records in the tile index
                    for g in grid:
                        poly = shape(g['geometry'])
                        if geom.intersects(poly):
                            filename = filename + " " + g['properties']['location']
    print filename
 

def main():
    parser = argparse.ArgumentParser(
        usage = 'python %s tileIndex aoi output' % __file__,
        formatter_class = argparse.RawDescriptionHelpFormatter,
        description=__doc__)

    parser.add_argument("tileIndex", type=str,
        help="The geoJSON tile index")
    parser.add_argument("aoi", type=str,
        help="The area of interest to overlay against the tile index")

    args = parser.parse_args()
    createMosaic(args.tileIndex, args.aoi)


if __name__ == "__main__" :
    main()
