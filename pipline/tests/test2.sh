gdalwarp -cutline /gpfs_scratch/ned_data_boundaries/State/geojson/RI.geojson -crop_to_cutline -of VRT            /gpfs_scratch/usgs/ned10m/ned10m.vrt RI.vrt
gdalwarp -t_srs "EPSG:26957" -of VRT RI.vrt RI_repro.vrt
gdalwarp RI_repro.vrt RI.tif
gdaldem hillshade RI.tif RI_hillshade.tif
python 1/cybergis-viz/VizMain.py RI_hillshade.tif
