""" 
A python script to generate a workflow execution bash script based on a 
json config file.
"""
import sys
import os.path
import argparse
from Configuration import Configuration

def createScript(configfile, script):
    """
    Creates the workflow execution bash script

    Parameters:
    -----------
    configFile:     - Config containing parameters to generate script from
    script:         - Name of the output script
    """
    config = Configuration(configfile)
    wdir = os.path.dirname(__file__)
    shapefile = config.getShapeFile()
    layername = os.path.split(shapefile)[-1].split(".")[0]
    script = open(script, 'w')

    src_vrt = os.path.join(wdir, layername+".vrt")
    dst_vrt = os.path.join(wdir, layername+"_repro.vrt")

    cmd = "mkdir /projects/geoserver/job_{}".format(layername)
    script.write(cmd + "\n")
    cmd = "cd /projects/geoserver/job_{}".format(layername)
    script.write(cmd + "\n")

    # Mosaic and Clipping
    cmd = "gdalwarp -cutline {} -crop_to_cutline -of VRT \
           /gpfs_scratch/usgs/ned10m/ned10m.vrt {}" \
           .format(shapefile, src_vrt)
    script.write(cmd + "\n")

    # Resampling and Reprojection
    resample = config.getResample()
    if resample: 
        resampleunit = config.getResampleUnit()
        if resampleunit == "unit": 
            resamarg = "-tr"
        elif resampleunit == "pixel":
            resamarg = "-ts"
        else:
            sys.exit("Invalid resample method")
        resamplex = config.getResampleX()
        resampley = config.getResampleY()

    reproject = config.getReproject()
    if resample and reproject:
        cmd = 'gdalwarp -t_srs "{}" {} {} {} -r {} -of VRT {} {}' \
               .format(config.getCoordinateSystem(), resamarg, resamplex, \
                       resampley, config.getResampleMethod(), src_vrt, \
                       dst_vrt)
    elif resample:
        cmd = 'gdalwarp {} {} {} -r {} -of VRT {} {}' \
               .format(resamarg, resamplex, resampley, \
               config.getResampleMethod(), src_vrt, dst_vrt)
    elif reproject:
        cmd = 'gdalwarp -t_srs "{}" -of VRT {} {}'\
                .format(config.getCoordinateSystem(), src_vrt, dst_vrt)
    else:
        cmd = "gdalwarp -of VRT {} {}"\
                .format(src_vrt, dst_vrt)
    script.write(cmd + "\n")

    # Converting from a virtual raster into a GeoTiff file
    outformat = config.getFormat()
    if outformat.lower() == "tif":
        output = os.path.join(wdir, layername+".tif")
    else:
        sys.exit("Invalid output format, should be tif")
    cmd = "gdalwarp {} {}".format(dst_vrt, output)
    script.write(cmd + "\n")
 
    # Add pyramids if the size is greater than 2G
    cmd = "fSize=$(stat -c%s \"{}\")".format(output)
    script.write(cmd + "\n")
    script.write("if [ $fSize -ge 2000000000 ]; then" + "\n")
    script.write("\tmkdir pyramids" + "\n")
    cmd = "gdal_retile.py -levels 4 -ps 2048 2048 -targetDir pyramids {}".\
          format(layername)
    script.write("\t" + cmd + "\n")
 
    # Publish the DEM raster onto GeoServer 
    #vizmain = os.path.join(wdir, "cybergis-viz/VizMain.py")
    #cmd = "python {} {}".format(vizmain, output)
    #script.write(cmd + "\n")

    # Enquire for preference on the derived products
    if config.getProductsSlope() == True:
        output_slope = os.path.join(wdir, layername+"_slope.tif")
        cmd = "gdaldem slope {} {}".format(output, output_slope)
        script.write(cmd + "\n")
        # Publish the slope raster and clean up
        #script.write("python {} {}".format(vizmain, output_slope) + "\n")
        #script.write("rm {}".format(output_slope) + "\n")

    if config.getProductsHillshade() == True:
        output_hillshade = os.path.join(wdir, layername+"_hillshade.tif")
        cmd = "gdaldem hillshade {} {}".format(output, output_hillshade)
        script.write(cmd + "\n")
        #script.write("python {} {}".format(vizmain, output_hillshade) \
#                     + "\n")
        #script.write("rm {}".format(output_hillshade) + "\n")
   
    if config.getProductsPitremove() == True:
        output_pitremove = os.path.join(wdir, layername+"_pitremove.tif")
        script.write("mkdir pitrm" + "\n")
        script.write("cp {} pitrm/".format(output) + "\n")
        procs = 16 # hard code for now
        script.write("mpirun -np {} pitremove pitrm".format(procs) + "\n")
        #script.write("python {} pitrmfel/*.tif".format(vizmain) + "\n")
        #script.write("rm -rf pitrm pitrmfel" + "\n")   

    # Clean up
    cmd = "rm {} {}".format(src_vrt, dst_vrt) 
    #cmd = "rm {} {} {}".format(src_vrt, dst_vrt, output)
    script.write(cmd + "\n")

    script.close()


def main():
    parser = argparse.ArgumentParser(
        usage = 'python %s config_json output' %__file__,
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=__doc__)

    parser.add_argument("config_json", type=str,
        help="Input configuration json file")
    parser.add_argument("output_script", type=str,
        help="Name of the output script")

    args = parser.parse_args()
    createScript(args.config_json, args.output_script)

if __name__ == '__main__':
    main()
