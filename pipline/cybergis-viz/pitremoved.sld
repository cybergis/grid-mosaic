<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.0.0/StyledLayerDescriptor.xsd" version="1.0.0">
<UserLayer>
<Name>raster_color</Name>
<LayerFeatureConstraints>
<FeatureTypeConstraint/>
</LayerFeatureConstraints>
<UserStyle>
<Name>raster</Name>
<Title>A very simple color map</Title>
<Abstract>A very basic color map</Abstract>
<FeatureTypeStyle>
<FeatureTypeName>Feature</FeatureTypeName>
<Rule>
<RasterSymbolizer>
    <Opacity>1.0</Opacity>
    <ColorMap extended="true">
 	<ColorMapEntry color="#ffffff" quantity="-3.0000000055e+38" label="NoData: -3.0000000055e+38" opacity="0"/>
	<!--<ColorMapEntry color="#CCCCFF" quantity="1424.15"/>-->
        <ColorMapEntry color="#009999" quantity="1615.174"/>
        <ColorMapEntry color="#99FF66" quantity="1806.198"/>
        <ColorMapEntry color="#FFFF66" quantity="1997.222"/>
        <ColorMapEntry color="#FFFF00" quantity="2188.245"/>
        <ColorMapEntry color="#FF9900" quantity="2379.269"/>
        <ColorMapEntry color="#FF6600" quantity="2570.293"/>
        <ColorMapEntry color="#FF6666" quantity="2761.317"/>
        <ColorMapEntry color="#FF3300" quantity="2952.34"/>
        <ColorMapEntry color="#CC33FF" quantity="3143.364"/>
        <ColorMapEntry color="#FF33FF" quantity="3143.364"/>
	<!--Higher-->
    </ColorMap>
</RasterSymbolizer>

</Rule>
</FeatureTypeStyle>
</UserStyle>
</UserLayer>
</StyledLayerDescriptor>
