import os
import logging
import urllib2

"""
Utilities function set to abstract commonly used functions
@author: Guofeng Cao based on online copyright free codes
"""
def run(cmd, 
        stdout=None,
        stderr=None, 
        verbose=False, logger = None):
    """Run command with or without echoing
    Possible redirect stdout and stderr
    """
    if verbose and logger is not None:
        logger.info("Excute the system command %s. \n" %cmd)
            
    s = cmd    
    if stdout:
        s += ' > %s' % stdout
        
    if stderr:
        s += ' 2> %s' % stderr        
        
    err = os.system(s)
    
    if err != 0:
        msg = 'Command "%s" failed with errorcode %i. ' % (cmd, err)
        if logger is not None:
            logger.error(msg)
        if stdout and stderr: msg += 'See logfiles %s and %s for details' % (stdout, stderr)
        raise Exception(msg)

    return err



def get_web_page(url, username=None, password=None):
    """Get url page possible with username and password
    """

    if username is not None:

        # Create password manager
        passman = urllib2.HTTPPasswordMgrWithDefaultRealm()
        passman.add_password(None, url, username, password)

        # create the handler
        authhandler = urllib2.HTTPBasicAuthHandler(passman)
        opener = urllib2.build_opener(authhandler)
        urllib2.install_opener(opener)
    pagehandle = urllib2.urlopen(url)
    page = pagehandle.readlines()

    return page

    
def curl(url, username, password, request, content_type, rest_dir, data_type, data, verbose=False, logger = None):
    """Aggregate and issue curl command:
    
    curl -u username:password -X request -H type geoserver_url/rest/rest_dir data_type data
    For example:
    
    curl -u admin:geoserver -X POST -H "Content-type: text/xml" "http://localhost:8080/geoserver/rest/workspaces" --data-ascii "<workspace><name>futnuh</name></workspace>"
    
    or 
    
    curl -u admin:geoserver -X PUT -H "image/tif" http://localhost:8080/geoserver/rest/workspaces/futnuh/coveragestores/shakemap_padang_20090930/file.geotiff  --data-binary @./data/shakemap_padang_20090930.tif
    
    """
    cmd = 'curl -u %s:%s' % (username, password)
    if verbose:
        cmd += ' -v'
    
    cmd += ' -X %s' % request
    cmd += ' -H "Content-type: %s"' % content_type     
    cmd += ' "%s"' % os.path.join(url, 'rest', rest_dir)
    if data_type:
        cmd += ' %s' % data_type
        
        assert len(data) > 0
        cmd += ' "%s"' % data

        
    curl_stdout = None#os.path.dirname(__file__) + "/logs/curl.stdout"    
    curl_stderr = None#os.path.dirname(__file__) + "/logs/curl.stderr"
    run(cmd, curl_stdout,curl_stderr, verbose=verbose, logger=logger)
