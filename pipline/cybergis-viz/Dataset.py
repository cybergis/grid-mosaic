import logging
from VizConfiguration import VizConfiguration

class Dataset:
    """
    An abstract encapsulation of GIS dataset, please call its child classes rasterdataset and vectordataset to access/query the data
    @author Guofeng Cao
    @todo: put datasource information into a separate class to manage the datasets collections
    """
    RASTER=0
    POINT =1
    LINE  =2
    POLYGON =3
    ATTRIBUTE= 100
    UNKOWN = -1
    """
    Database type, raster =0, point = 1, line = 2, polygon=3, attribute table = 100
    """
    
    LOCALFILE = 0
    DATABASE = 1
    
    def __init__(self, dsName, dtName):
        self._config= VizConfiguration.getInstance()
        self._logger = logging.getLogger(self._config.getAppName())
        self._bounds = None
        self._type = None
        self._projection = None
        # data set 
        self._dtName = dtName
        # data source
        self._dsName = dsName
        self._datasource= None
        self._dataset = None
        self._filetype = None
        
        
    def open(self,dsName,dtName):
        self._dsName = dsName
        self._dtName = dtName
        
    def close(self):
        self._bounds = None
        self._type = None
        self._projection = None
        self._datasetName = None
        self._dsName = None
        # data source name
        self._datasource= None
        self._config = None
         
        """
        for file based data, dsName returns the full path
        """
    def getDsName(self):
        if not self._dataset is None:
            return self._dsName
        """
        for file based data, dtName returns the name of dataset, path, extension info excluded
        """
    def getDtName(self):
        if not self._dataset is None:
            return self._dtName
    def getType(self):
        if not self._dataset is None:
            return self._type
    def getFileType(self):
        if not self._dataset is None:
            return self._filetype
        
    def getProjection (self):
        raise NotImplementedError
    def getMax(self):
        raise NotImplementedError 
    def getMin(self):
        raise NotImplementedError 
    def getBounds(self):
        raise NotImplementedError 
    def query(self, bb,filter):
        raise NotImplementedError
    