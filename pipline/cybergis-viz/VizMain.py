from RasterDataset import RasterDataset,FileRasterDataset
from VectorDataset import OgrVectorDataset
from VizConfiguration import VizConfiguration
from GeoserverConnector import GeoserverConnector
import os
import re
import ogr
import sys
import json
import subprocess
from osgeo import gdal, gdalconst

def generateMetaData(tiffile, geoserver, layerName):
    wmsUrl = geoserver.getWMSPrefix()+"tiles=true&"
    wmsParams = layerName
    service = {"url":wmsUrl, "params":wmsParams}

    info = subprocess.check_output(["gdalinfo", "-stats", tiffile]).split("\n")

    stat = re.search('\w+=(\S+), \w+=(\S+), \w+=(\S+), \w+=(\S+)', info[-8])
    statAll = {"Min": float(stat.group(1)), "Max": float(stat.group(2)), \
             "Mean": float(stat.group(3)), "Std Dev": float(stat.group(4))}
    
    rowCol = map(int, re.findall(r'\d+', info[3]))
    fSize = round(os.path.getsize(tiffile)/1000000.0, 2)
    noData = float(info[-7].split('=')[1])
    rasInfo = {"Rows":rowCol[1], "Columns":rowCol[0], \
               "Output Size (MB)":fSize, "NoData Value":noData}
    print rasInfo         
    data = {"GeoServer Service Info":service, "Raster Info":rasInfo, \
            "Statistics": statAll}
    output = os.path.join("..", layerName + "_metadata.json")
    with open(output, 'w') as outfile:
        json.dump(data, outfile, indent=4)

class VizMain:
    @classmethod  
    def _prepareNedKDE(cls, tiffile):
        geoserver = GeoserverConnector.getInstance()
        geoserver.createWorkspace("UsgsNedDataApp")
        layerName = os.path.splitext(tiffile)[0].split("/")[-1]
        dataset = FileRasterDataset(tiffile)
        ret = geoserver.addLayer(dataset,layerName,None, "flukde")
        if ret == False:
            print "Could not add layer {}.".format(layerName)
            print "Check whether layer {} already exists.".format(layerName)
        #generateMetaData(tiffile, geoserver, layerName)

    @classmethod
    def startProxy(cls, tiffile):
        VizMain._prepareNedKDE(tiffile)

def main(tiffile):
    VizMain.startProxy(tiffile)
#    print geos.getWMSPrefix()+"request=GetMap&service=WMS&version=1.1.1&layers=RI_hillshade&srs=EPSG%3A4326&bbox=-71.94421724874401,41.14768429666641,-71.0629010041388,42.0166174621344&&width=780&height=330&format=image%2Fpng"

if __name__ == "__main__":
    main(sys.argv[1])
