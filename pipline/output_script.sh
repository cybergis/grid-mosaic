gdalwarp -cutline /gpfs_scratch/ned_data_boundaries/State/geojson/RI.geojson -crop_to_cutline -of VRT            /gpfs_scratch/usgs/TileStitching/ned10m.tif RI.vrt
gdalwarp -t_srs "EPSG:26957" -of VRT RI.vrt RI_repro.vrt
gdalwarp RI_repro.vrt RI.tif
gdaldem hillshade RI.tif RI_hillshade.tif
python cybergis-viz/VizMain.py RI_hillshade.tif
rm RI_hillshade.tif
rm RI.vrt RI_repro.vrt RI.tif

cmd="mpirun_rsh -np $np -hostfile $PBS_NODEFILE $HOME/TauDEM/pitremove" +
    "-mf 1 1 -z ${ddir} -fel ${wdir}fel "

mpirun -np 5 
