#! /bin/bash
# workflow script for UsgsNedApp

if [ $# -gt 1 ]; then
    echo "Usage: $0 [configuration json file]"
    echo -e "\t configuration json file: defaults to config.json"
    exit 1    
fi

#---env setup---
if [ -z "$GDAL_HOME" ]; then 
    echo "INFO: Cannot find GDAL. Loading to continue execution."
    module load gdal-stack
    if  [ -z "$GDAL_HOME" ]; then
        echo "ERROR: cannot load GDAL"
        exit 1
    fi
fi

if [ -z "$TAUDEM_HOME" ]; then
    echo "INFO: Cannot find TauDEM. Loading to continue execution."
    module load taudem
    if [ -z "$GDAL_HOME" ]; then
        echo "ERROR: cannot load TauDEM"
        exit 1
    fi
fi

# workspace setting
wdir="."
rdir="$wdir/results"
if [ ! -d $rdir ]; then
    mkdir -p $rdir
fi

configfile="$wdir/config.json"
if [ $# -eq 1 ]; then
    configfile=$1
fi
if [ ! -f $configfile ]; then
    echo "ERROR: Configuration file $configfile does not exist"
    exit 1
fi

scriptfile="$wdir/pipline.sh"
python cmdseqgen.py $configfile $scriptfile
if [ ! -f $scriptfile ]; then
    echo "ERROR: Cannot create script file $scriptfile"
    exit 1
fi
#sh $scriptfile

# Clean up 
#rm $scriptfile
