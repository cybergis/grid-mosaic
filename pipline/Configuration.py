import sys
import json
import os.path


class Configuration():
    """
    A class to access the configuration of
    All the members should be taken as read-only and access through associated get functions
    Any assignment operation is prohibited. Please see the test code for usages.
    @author: Guofeng Cao
    @see: Singleton.py  
    """
    def __init__(self, filename):
        
        self._config_file = os.path.join(os.path.dirname(__file__), filename)
        
        fjson = open(self._config_file, 'r')
        self._config = json.load(fjson)
        fjson.close()

        self._shapeFile = self._config["Input"]["ShapeFile"]

        self._resample = self._config["Output"]["Resample"]
        self._resample_method= self._config["Output"]["ResampleMethod"]
        self._resample_unit= self._config["Output"]["ResampleUnit"]
        self._resample_x = float(self._config["Output"]["X"])
        self._resample_y = float(self._config["Output"]["Y"])
        self._reproject = float(self._config["Output"]["Reproject"])
        self._cs = self._config["Output"]["CoordinateSystem"]
        self._format = self._config["Output"]["Format"]
        
        self._products_dem = self._config["Products"]["Dem"]
        self._products_slope = self._config["Products"]["Slope"]
        self._products_hillshade = self._config["Products"]["Hillshade"]
        self._products_pitremove = self._config["Products"]["Pitremove"]
        self._arcgis = self._config["Products"]["PublishToArcgisOnline"]
        if self.validate() != True:
            sys.exit("Error in configuration file")
        
    def validate(self):
        ret = True
        if self._cs.find(":") == -1: 
            print "Invalid coordinate system in configuration file"
            ret = False
        return ret

    def getShapeFile(self):
        return self._shapeFile
    
    def getResample(self):
        return self._resample
    
    def getResampleMethod(self):
        return self._resample_method

    def getResampleUnit(self):
        return self._resample_unit

    def getResampleX(self):
        return self._resample_x
 
    def getResampleY(self):
        return self._resample_y
    
    def getReproject(self):
        return self._reproject

    def getCoordinateSystem(self):
        return self._cs
    
    def getFormat(self):
        return self._format
    
    def getProductsDem(self):
        return self._products_dem

    def getProductsSlope(self):
        return self._products_slope

    def getProductsHillshade(self):
        return self._products_hillshade

    def getProductsPitremove(self):
        return self._products_pitremove

    def getArcgisPublish(self):
        return self._arcgis

if __name__ == "__main__":
    test=Configuration("config.json")
    print test.getArcgisPublish()
    print test.getProductsPitremove()
    
        
