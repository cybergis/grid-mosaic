## Grid_Mosaic ##

### Authors: ###
Sunwoo Kim <kim392@illinois.edu>

### Summary ###
The user will specify an input directory containing raster files. These files will behave as vector polygon tiles for the tile index. Once the final tile index is produced, it will be returned in geoJSON format.

After creating the tile index, an input vector file will be overlaid against it and the intersecting tiles will be derived from this process. A virtual raster comprised of the intersecting tiles will be returned as a final product.
### Requirements ###
* gdal-stack
* python/2.7.0

### Content ###
* src/
	- create_grid.py: Creates tile index geoJSON
	- mosaic.py: Overlaps input vector file with the tile index and returns a virtual raster composed of the intersecting tiles.
* test/