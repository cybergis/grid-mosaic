import os
import re
import json
import rasterio

def writeCollection(dir, outFile):
    """ Writes the coordinates of raster files in the input directory into
        a geoJSON file

    Keyword Argument:
    dir:        -- Input directory containing raster files
    outFile:    -- Name of the output geoJSON
    """
    file = open(outFile, "w")
    
    feature_collection = {
                              "type": "FeatureCollection",
                              "features": []
                         }
    for fn in os.listdir(dir):
        print "Reading " + fn + "..."
        try:
            feat = writeFeature(dir+fn)
            feature_collection["features"].append(feat)

        # If the file is not in an acceptable format
        except IOError: 
            print "I/O Error reading file " + fn
    json.dump(feature_collection, file, indent=4)

    file.close()
    print "Done writing grid to " + outFile

def writeFeature(fn):
    """ Writes coordinates for the geoJSON file

    Keyword Argument:
    fn:         -- Filename
    """
    data = getBB(fn)
    coords = getCO(data)
    feat = {
                'type': 'Feature', 
                'bbox': [data[0], data[1], data[2], data[3]],
                'properties':{'title': fn},
                'geometry':{'type': 'Polygon', 'coordinates': coords}
           }
    return feat

def getBB(fn):
    """ Utilizes RasterIO's bounds function to get the bounding box

    Keyword Argument:
    fn:         -- Filename
    """
    with rasterio.drivers():
        with rasterio.open(fn) as src:
            data = src.bounds
            data = re.sub('[^0-9.,-]', '', str(data)).split(',')
            return map(float,data)

def getCO(data):
    """ Returns a set of coordinate points from the bounding box

    Keyword Argument:
    data:       -- The bounding box of the four corners 
    """
    coords = []
    coArray = []
    coArray.append([data[0], data[1]])
    coArray.append([data[2], data[1]])
    coArray.append([data[2], data[3]])
    coArray.append([data[0], data[3]])
    coArray.append([data[0], data[1]])
    coords.append(coArray)
    return coords
