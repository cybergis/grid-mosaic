from util_grid import *

def checkTiles(tileList, output):
    """ Write a geoJSON file from the files listed in the textfile from
        mosaic.py

    Keyword Arguments:
    tileList:       -- List of tiles 
    output:         -- Name of output geoJSON
    """
    json_file = open(output, "wb")
    feature_collection = {
                            "type": "FeatureCollection",
                            "features": []
                         }

    file = open(tileList, 'r')
    for line in file:
        line = line.strip()
        print "Reading " + line + "..."
        feat = writeFeature(line)
        feature_collection["features"].append(feat)
    file.close()

    json.dump(feature_collection, json_file, indent=4)
    json_file.close()

    print "Done writing test mosaic to test.geojson"
