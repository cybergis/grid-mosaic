"""
Get a geoJSON out of the list of tiles textfile outputted from mosaic.py
"""
import argparse

from util_check import *

def parseArguments():
    parser = argparse.ArgumentParser(
        usage = 'python %s input output' % __file__,
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=__doc__)
    parser.add_argument("input", type=str,
        help="list of tiles from mosaic.py")
    parser.add_argument("output", type=str,
        help="output geoJSON")

    return parser.parse_args()

def main():
    args = parseArguments()
    checkTiles(args.input, args.output)

if __name__ == "__main__" :
    main()
